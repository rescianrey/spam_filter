import configparser
import math
import os
import re
import sys

PROBABILITY_TABLE_FILE = './probability_table'
CONFIG_FILE = './spam_filter.cfg'

class SpamFilter:
    def __init__(self):
        self.read_config()
        self.class_map = None
        self.prior_probability_table = {}
        self.spam_priori = 0.0
        self.ham_priori = 0.0

    def read_config(self):
        print('Reading config file...')
        config = configparser.ConfigParser()
        config.read(CONFIG_FILE)
        self.data_dir = config.get('configuration', 'data_directory')
        self.training_set_limit = config.getint('configuration', 'training_set_limit')
        self.labels_file =  config.get('configuration', 'labels_file')
        self.lambda_value = config.getfloat('configuration', 'lambda')
        self.minimum_word_frequency =  config.getint('configuration', 'minimum_word_frequency')
        self.top_n_frequently_used_words_to_remove =  config.getint('configuration', 'top_n_frequently_used_words_to_remove')
        self.top_n_MI_score_to_include =  config.getint('configuration', 'top_n_MI_score_to_include')

        print('Data directory: %s' % self.data_dir)
        print('Labels file: %s' % self.labels_file)
        print('Training set partition limit: %s' % self.training_set_limit)
        print('Lambda: %s' % self.lambda_value)
        print('Allow removal of less frequently used words? %s, value: %s' % (self.minimum_word_frequency != 0, self.minimum_word_frequency))
        print('Allow removal of most frequently used words? %s, value: %s' % (self.top_n_frequently_used_words_to_remove != 0, self.top_n_frequently_used_words_to_remove))
        print('Allow using only top informative words? %s, value: %s' % (self.top_n_MI_score_to_include != 0, self.top_n_MI_score_to_include))
        
    def train(self):
        print('Training started...')
        training_set = self.get_dataset('training')
        total_docs, spam_count, ham_count, vocabulary = self.get_training_set_statistics(training_set)

        self.ham_count = ham_count
        self.spam_count = spam_count
        self.spam_priori = float(spam_count) / total_docs
        self.ham_priori = float(ham_count) / total_docs

        vocabulary = self.remove_top_n_frequently_used_words(vocabulary)
        vocabulary = self.remove_less_frequently_used_words(vocabulary)
        self.populate_prior_probability_table(vocabulary)
        self.include_only_top_n_MI_score()
        self.save_probability_table()

        print('Training done.')
        
    def get_training_set_statistics(self, training_set):
        print('Getting training set statistics...')
        spam_count = 0
        ham_count = 0
        vocabulary = {}
        for document in training_set:
            document_class = self.get_doc_class_from_labels(document)
            is_document_spam = False
            if document_class == 'spam':
                spam_count += 1
                is_document_spam = True
            else:
                ham_count += 1
            content = self.get_words_from_file(document)
            for word in content:
                if word not in vocabulary:
                    vocabulary[word] = {'spam': 0, 'ham': 0, 'frequency': 0}
                if is_document_spam:
                    vocabulary[word]['spam'] += 1
                else:
                    vocabulary[word]['ham'] += 1
                vocabulary[word]['frequency'] += 1
        total = spam_count+ham_count
        print('Statistics: total-%s spam-%s ham-%s vocabulary_size-%s' % (total, spam_count, ham_count, len(vocabulary)))
        return (total, spam_count, ham_count, vocabulary)

    def populate_prior_probability_table(self, vocabulary):
        print('Populating prior probability table...')
        vocabulary_size = len(vocabulary)
        for word in vocabulary:
            self.prior_probability_table[word] = {}
            self.prior_probability_table[word]['ham'] = float(vocabulary[word]['ham'] + self.lambda_value)/ (self.ham_count + self.lambda_value*vocabulary_size)
            self.prior_probability_table[word]['spam'] = float(vocabulary[word]['spam'] + self.lambda_value)/ (self.spam_count + self.lambda_value*vocabulary_size)
        print('Population done.')

    def save_probability_table(self):
        print('Saving probability table...')
        table_line_format = '{:<30} {:<30} {:<30}\n'
        config_file = open(PROBABILITY_TABLE_FILE, 'w')
        config_file.write(table_line_format.format('-', 'spam', 'ham'))
        config_file.write(table_line_format.format('PRIORI', self.spam_priori, self.ham_priori))
        for word in sorted(self.prior_probability_table):
            config_file.write(table_line_format.format(word, self.prior_probability_table[word]['spam'], self.prior_probability_table[word]['ham']))
        config_file.close()
        print('Saving done.')

    def read_probability_table(self):
        print('Reading probability table...')
        config_file = open(PROBABILITY_TABLE_FILE, 'r')
        config_file.readline()
        self.spam_priori, self.ham_priori = [float(p) for p in config_file.readline().split()[1:]]
        self.prior_probability_table = {}
        for line in config_file:
            word, spam_priori, ham_priori = line.split()
            self.prior_probability_table[word] = {'spam': float(spam_priori), 'ham': float(ham_priori)}
        print('Reading probability table done.')

    def is_spam(self, filepath):
        content = self.get_words_from_file(filepath)
        spam_probability = self.get_class_probability(content, 'spam')
        ham_probability = self.get_class_probability(content, 'ham') 
        return 'spam' if spam_probability > ham_probability else 'ham'

    def get_class_probability(self, content, classname):
        priori = 0.0
        if classname == 'spam':
            priori = self.spam_priori
        else:
            priori = self.ham_priori

        probability_base_e = math.log(priori)
        for word in self.prior_probability_table:
            word_prior_probability = 0.0
            if word in content:
                word_prior_probability = self.prior_probability_table[word][classname]
            else:
                word_prior_probability = 1 - self.prior_probability_table[word][classname]
            probability_base_e += math.log(word_prior_probability)
        return math.exp(probability_base_e)

    def get_dataset(self, settype):
        print('Querying for %s data...' % settype)
        dataset_files = []
        data_directories = None
        for (current_dir, directories, files) in os.walk(self.data_dir):
            data_directories = directories
            break

        partitioned_dataset = None
        if settype == 'training':
            partitioned_dataset = data_directories[:self.training_set_limit+1]
        else:
            partitioned_dataset = data_directories[self.training_set_limit+1:]

        for directory in partitioned_dataset:
            dirpath = os.path.join(self.data_dir, directory)
            dataset_files += self.get_directory_files(dirpath)
        print('%s files queried.' % len(dataset_files))
        return dataset_files

    def get_directory_files(self, dirpath):
        return [os.path.join(dirpath, f) for f in os.listdir(dirpath) if not f.startswith('.')]

    def get_class_map(self):
        self.class_map = {}
        labels_file = open(self.labels_file, 'r')
        for line in labels_file:
            classification, filepath = line.split()
            map_key = self.get_class_map_key(filepath)
            self.class_map[map_key] = classification
        labels_file.close()

    def get_doc_class_from_labels(self, training_set_item_fpath):
        if self.class_map is None:
            self.get_class_map()
        map_key = self.get_class_map_key(training_set_item_fpath)
        return self.class_map[map_key]

    def get_class_map_key(self, training_set_item_fpath):
        key = training_set_item_fpath.replace(self.data_dir, '')
        key = key.replace('/', '')
        key = key.replace('\\', '')
        key = key.replace('.', '')
        return key

    def remove_top_n_frequently_used_words(self, vocabulary):
        if not self.top_n_frequently_used_words_to_remove:
            return vocabulary
        print('Removing top %s frequently used words from vocabulary...' % self.top_n_frequently_used_words_to_remove)
        vocabulary_sorted_by_freq = sorted([(word, vocabulary[word]['frequency']) for word in vocabulary], key=lambda x:x[1])
        top_n_removed = vocabulary_sorted_by_freq[:-self.top_n_frequently_used_words_to_remove]
        filtered_vocabulary = {}
        for word, frequency in top_n_removed:
            filtered_vocabulary[word] = vocabulary[word]
        print('Removed. New vocabulary size: %s' % len(filtered_vocabulary))
        return filtered_vocabulary

    def remove_less_frequently_used_words(self, vocabulary):
        if not self.minimum_word_frequency:
            return vocabulary
        print('Removing less frequently used words from vocabulary. Minimum requirement %s...' % self.minimum_word_frequency)
        filtered_vocabulary = {}
        for word in vocabulary:
            if vocabulary[word]['frequency'] >= self.minimum_word_frequency:
                filtered_vocabulary[word] = vocabulary[word]
        print('Removed. New vocabulary size: %s' % len(filtered_vocabulary))
        return filtered_vocabulary

    def include_only_top_n_MI_score(self):
        if not self.top_n_MI_score_to_include:
            return
        print('Filtering words by mutual information...')
        print('Only top %s words with high mutual information will be included.' % self.top_n_MI_score_to_include)

        vocabulary_sorted_by_MI = sorted([(word, self.get_mutual_information(word)) for word in self.prior_probability_table], key=lambda x:x[1])

        temp_priori_probability_table = {}
        for word, score in vocabulary_sorted_by_MI[-self.top_n_MI_score_to_include:]:
            print(word)
            temp_priori_probability_table[word] = self.prior_probability_table[word]
        self.prior_probability_table = temp_priori_probability_table
        print('Filtering words by mutual information done.')
        return

    def get_mutual_information(self, word):
        mutual_info_score = 0.0
        prob_of_word = self.prior_probability_table[word]['spam'] + self.prior_probability_table[word]['ham']
        prob_of_class = {'spam': self.spam_priori, 'ham': self.ham_priori}
        for x in [0, 1]:
            prob_x = prob_of_word if x else (1 - prob_of_word)
            for c in prob_of_class:
                prob_of_x_given_c = self.prior_probability_table[word][c] if x else (1 - self.prior_probability_table[word][c])
                mutual_info_score += prob_of_x_given_c * math.log(prob_of_x_given_c/(prob_x * prob_of_class[c]))
        return mutual_info_score

    def get_words_from_file(self, filepath):
        file_content = None 
        try:
            file_p = open(filepath)
            file_content = file_p.read()
        except UnicodeDecodeError:
            file_p = open(filepath, encoding='latin-1')
            file_content = file_p.read()
        return self.get_words_from_string(file_content)

    def get_words_from_string(self, file_content):
        WORD_PATTERN = '[a-zA-Z]{2,}'
        file_content = self.clean_content(file_content)
        words = set([ w.lower() for w in re.findall(WORD_PATTERN, file_content) ])
        return words

    def clean_content(self, file_content):
        cleaned_version = self.remove_punctuations(file_content)
        cleaned_version = self.remove_html_tags(file_content)
        return cleaned_version

    def remove_punctuations(self, file_content):
        # PUNCTUATION_MARKS = ['.', '!', ':', ',', '?', ';']
        PUNCTUATION_MARKS = ['.', ',']
        punctuation_regex = '[%s]' % ''.join(PUNCTUATION_MARKS)
        cleaned_version = re.sub(punctuation_regex, ' ', file_content)
        return cleaned_version

    def remove_html_tags(self, file_content):
        HTML_TAG_PATTERN = '</?.+?>'
        cleaned_version = re.sub(HTML_TAG_PATTERN, ' ', file_content)
        return cleaned_version

    def pretty_print_dict(self, dictionary):
        for key in sorted(dictionary):
            print('{:<30} {:<30}'.format(key, dictionary[key]))

    def run_against_test_set(self, test_set):
        true_pos = 0
        true_neg = 0
        false_pos = 0
        false_neg = 0

        if not self.prior_probability_table:
            self.read_probability_table()

        print('Running against %s test items...' % len(test_set))
        for test_set_item in test_set:
            expected = self.get_doc_class_from_labels(test_set_item)
            actual = self.is_spam(test_set_item)

            if expected == 'spam' and actual == 'spam':
                true_pos += 1
            elif expected == 'ham' and actual == 'ham':
                true_neg += 1
            elif expected == 'ham' and actual == 'spam':
                false_pos += 1
            else:
                false_neg += 1

        precision = float(true_pos) / (true_pos + false_pos)
        recall = float(true_pos) / (true_pos + false_neg)
        print('\n')
        print('Results:')
        print('TP:%s TN:%s FP:%s FN:%s' % (true_pos, true_neg, false_pos, false_neg))
        print('Precision: %s' % precision)
        print('Recall: %s' % recall)

        return (precision, recall)
 
if __name__ == '__main__':
    args = sys.argv[1:]
    filter = SpamFilter()

    # can make training optional if you already run it before
    if not (args and args[0] == '--skip-training'):
        filter.train()

    # can make running on test items optional if you only want to train
    if not (args and args[0] == '--training-only'):
        test_set = filter.get_dataset('test')
        filter.run_against_test_set(test_set)
