System Requirements:
- Python 3.x

How to run:
1. Open spam_filter.cfg and configure appropriately.
	- data_directory : the path where the training and test set is located
	- labels_file : the path of your labels file
	- training_set_limit: The last partition of your training set. This partitions your data from training set and test set. Partitions greater than this will be part of the test set.
	- lambda : the lamdba smoothing value of use.
	- top_n_frequently_used_words_to_remove: if you want to remove frequently used words, set this value. Otherwise, set this to 0.
	- minimum_word_frequency: if you want to remove less frequently used words, set this value. Otherwise, set this to 0.
	- top_n_MI_score_to_include: if you only want to use words with high mutual information, set this value. Otherwise set to 0.

2. Run python3 spam_filter.py

How it works:
- Once you run the program, it will train and construct the prior probabilities from the training set you provided in your configuration. It will also run the constructed classifier against the test set and output the precision and recall.
- You may skip the training if you have trained before by appending '--skip-training' in the command
- If you only want to train without running it against the test set, apped '--training-only' in the command