Reading config file...
Data directory: ./data
Labels file: ./labels
Training set partition limit: 70
Lambda: 1.0
Allow removal of less frequently used words? True, value: 20
Allow removal of most frequently used words? True, value: 100
Allow using only top informative words? False, value: 0
Querying for test data...
16522 files queried.
Reading probability table...
Reading probability table done.
Running against 16522 test items...


Results:
TP:9510 TN:5198 FP:189 FN:1625
Precision: 0.9805134549953604
Recall: 0.8540637629097441
