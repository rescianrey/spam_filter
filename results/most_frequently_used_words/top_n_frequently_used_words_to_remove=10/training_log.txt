Reading config file...
Data directory: ./data
Labels file: ./labels
Training set partition limit: 70
Lambda: 1.0
Allow removal of less frequently used words? True, value: 20
Allow removal of most frequently used words? True, value: 10
Allow using only top informative words? False, value: 0
Training started...
Querying for training data...
21300 files queried.
Getting training set statistics...
Statistics: total-21300 spam-13777 ham-7523 vocabulary_size-279040
Removing top 10 frequently used words from vocabulary...
Removed. New vocabulary size: 279030
Removing less frequently used words from vocabulary. Minimum requirement 20...
Removed. New vocabulary size: 11430
Populating prior probability table...
Population done.
Saving probability table...
Saving done.
Training done.
