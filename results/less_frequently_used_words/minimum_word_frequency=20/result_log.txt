Reading config file...
Data directory: ./data
Labels file: ./labels
Training set partition limit: 70
Lambda: 1.0
Allow removal of less frequently used words? True, value: 20
Allow removal of most frequently used words? False, value: 0
Allow using only top informative words? False, value: 0
Querying for test data...
16522 files queried.
Reading probability table...
Reading probability table done.
Running against 16522 test items...


Results:
TP:9355 TN:5095 FP:292 FN:1780
Precision: 0.9697315227531875
Recall: 0.8401436910642119
