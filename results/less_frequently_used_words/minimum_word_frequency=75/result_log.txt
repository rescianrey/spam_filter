Reading config file...
Data directory: ./data
Labels file: ./labels
Training set partition limit: 70
Lambda: 1.0
Allow removal of less frequently used words? True, value: 75
Allow removal of most frequently used words? False, value: 0
Allow using only top informative words? False, value: 0
Querying for test data...
16522 files queried.
Reading probability table...
Reading probability table done.
Running against 16522 test items...


Results:
TP:10076 TN:4999 FP:388 FN:1059
Precision: 0.9629204892966361
Recall: 0.9048944768747194
