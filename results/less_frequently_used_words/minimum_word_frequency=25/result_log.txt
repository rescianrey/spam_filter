Reading config file...
Data directory: ./data
Labels file: ./labels
Training set partition limit: 70
Lambda: 1.0
Allow removal of less frequently used words? True, value: 25
Allow removal of most frequently used words? False, value: 0
Allow using only top informative words? False, value: 0
Querying for test data...
16522 files queried.
Reading probability table...
Reading probability table done.
Running against 16522 test items...


Results:
TP:9491 TN:5082 FP:305 FN:1644
Precision: 0.9688648427929767
Recall: 0.8523574315222272
