Reading config file...
Data directory: ./data
Labels file: ./labels
Training set partition limit: 70
Lambda: 0.005
Allow removal of less frequently used words? True, value: 20
Allow removal of most frequently used words? True, value: 200
Allow using only top informative words? False, value: 0
Querying for test data...
16522 files queried.
Reading probability table...
Reading probability table done.
Running against 16522 test items...


Results:
TP:10003 TN:5175 FP:212 FN:1132
Precision: 0.9792462065589819
Recall: 0.8983385720700494
